package com.test.qa.automation.tests;

import com.test.qa.automation.MainTest;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.test.qa.automation.HomePage.*;
import static com.test.qa.automation.Utils.*;
import static com.test.qa.automation.Utils.driver;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author mykhail
 */
public class FirstTest extends MainTest {
    private static final Logger LOGGER = Logger.getLogger(FirstTest.class);

    @BeforeClass
    public void setUp() throws InterruptedException {
        driver = setupEnvironment();
    }

    @BeforeMethod
    public void openPage() throws InterruptedException {
        openURL(driver);
    }

    @AfterClass
    public void tearDown() {
//      driver.close();
        driver.quit();
    }
    @Test
    public void FirstTestStart () throws Exception{
        //String City = "Драгобрат";
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(header));// Header dropdown is clickable
        LOGGER.info("Header is visible");
        //LOGGER.info ("Header text :"  + " " + header.findElement(driver).getText());
        Assert.assertTrue(header.findElement(driver).getText().equals("Прогноз погоды")); // Прогноз погоды
        enterSearchRequest(driver, "Драгобрат");
        clickSearchButton(driver);
        openDayTab(driver);
        checkDayTab(driver);
        checkPrressureValue(driver);
        Thread.sleep(2000);
        LOGGER.info("Test is finished");
    }
}
