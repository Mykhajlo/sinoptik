package com.test.qa.automation;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;
import java.util.function.BooleanSupplier;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author mykhail
 */
public class HomePage {
    private static final Logger LOGGER = Logger.getLogger(HomePage.class);
    public static final By header = By.className("sLogo");
    public static final By search_field = By.id("search_city");
    public static final By search_data = By.className("isMain");
    public static final By search_button = By.className("search_city-submit");

    public static final By day_tab = By.xpath("//a[contains(@class, 'day-link')][contains(text(), 'Воскресенье')]");
    public static final By day_name = By.xpath("//p[contains(@class, 'infoDayweek')][contains(text(), 'Воскресенье')]");

    public static final By prressure = By.xpath("//*[@id=\"bd6c\"]/div[1]/div[2]/table/tbody/tr[5]//td[contains(@class, 'p')]");

    public  static void enterSearchRequest(WebDriver driver, String City) {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(search_field, "class","search_city")));
        if (driver.findElement(search_field).isDisplayed()){
            driver.findElement(search_field).sendKeys(City); // "Драгобрат"
            LOGGER.info("Search city is entered");
        }else {
            LOGGER.info("Bottom arrow button is not available");
        }
    }
    public  static void clickSearchButton(WebDriver driver)  {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.attributeContains(search_button, "class","search_city-submit"));
        if (driver.findElement(search_button).isDisplayed()){
            driver.findElement(search_button).click();// Click "Search"
            LOGGER.info("Button is clicked");
        }else {
            LOGGER.info("Button is not available");
        }
    }
    public  static void openDayTab(WebDriver driver)   {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        LOGGER.info("Search Data : " + search_data.findElement(driver).getText());
        wait.until(ExpectedConditions.invisibilityOfElementWithText(search_data, "Погода в Киеве"));
        //Thread.sleep(1000);
        driver.findElement(day_tab).click();// Click "Tab"
    }
    public  static void checkDayTab(WebDriver driver)  {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(day_name));
        if (driver.findElement(day_name).isDisplayed()){
            Assert.assertTrue(day_name.findElement(driver).getText().equals("Воскресенье"));
            LOGGER.info("Tab has correct name");
        }else {
            LOGGER.info("Tab has wrong name");
        }
    }
    public  static void checkPrressureValue(WebDriver driver)    {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(day_name));

        List<WebElement> webElementPrressure = driver.findElements(prressure);
        LOGGER.info("Total responses = " + webElementPrressure.size());
        int[] prrressure = new int[webElementPrressure.size()];
        Iterator<WebElement> iterator = webElementPrressure.iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            WebElement itemP= iterator.next();
            String labelP = itemP.getText();
            prrressure[i] = Integer.parseInt(labelP.trim());
            LOGGER.info("Results prrressure = " + prrressure[i]);
            Boolean position = prrressure[i] > 600 && prrressure[i] < 700;
            if (position){
                Assert.assertTrue(position);
                LOGGER.info("OK : " + prrressure[i] + " - inside the interval");
            } else {
                Assert.assertFalse(position);
                LOGGER.info("OK : " + prrressure[i] + " - outside the interval");
            }

        }

    }
}
