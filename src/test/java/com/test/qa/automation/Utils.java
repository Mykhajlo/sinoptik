package com.test.qa.automation;

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.test.qa.automation.HomePage.*;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author mykhail
 */
public class Utils {
    private static final Logger LOGGER = Logger.getLogger(Utils.class);
    public static WebDriver driver;
    public static ChromeDriver setupEnvironment() {
        //System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver.dmg");
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            System.setProperty("webdriver.chrome.driver", "src//test//resources//chromedriver_win.exe");
        }
        if (osName.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", "src//test//resources//chromedriver");
        } else if (osName.contains("linux")
                || osName.contains("mpe/ix")
                || osName.contains("freebsd")
                || osName.contains("irix")
                || osName.contains("digital unix")
                || osName.contains("unix")) {
            System.setProperty("webdriver.chrome.driver", "src//test//resources//chromedriver_linux64");
        }
        //linux
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        ChromeDriver driver = new ChromeDriver(capabilities);
        //ChromeDriver driver = new ChromeDriver();
        Dimension d = new Dimension(1366, 768); // > HD resolution
        driver.manage().window().setSize(d);
        //driver.manage().window().maximize(); // full size  of screen
        return driver;
    }
    public static void openURL(WebDriver driver) throws InterruptedException {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, SECONDS)
                .pollingEvery(500, MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        driver.get(getURL());
        driver.getWindowHandle();
        LOGGER.info("Test");
        wait.until(ExpectedConditions.visibilityOfElementLocated(header));// Location dropdown is clickable
        Assert.assertTrue(ofNullable(driver.findElement((header))).isPresent());
        LOGGER.info("Header field is present");
        LOGGER.info(driver.getCurrentUrl());
    }
    public static String getURL() {
        Properties prop = new Properties();
        InputStream input;
        String link = new String();
        try {
            input = new FileInputStream("src//test//resources//config");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            LOGGER.info(prop.getProperty("link"));
            link = prop.getProperty("link");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return link;
    }
}
