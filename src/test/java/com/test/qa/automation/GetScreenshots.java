package com.test.qa.automation;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author mykhail
 */
public class GetScreenshots extends  MainTest{
    private static final Logger LOGGER = Logger.getLogger(GetScreenshots.class);
    public static String failed(String screenshotName)throws IOException {
        LOGGER.info("Test is go " + driver.getTitle());
        File scrFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());
        //File scrName = new File("C:\\Users\\adyax-021\\IdeaProjects\\sinoptik\\src\\test\\screenshots\\" + "_" + screenshotName + "_" + timeStamp +".png");
        // File destination = new File (dest);
        String scrName = "src\\test\\screenshots\\" +  "_" + screenshotName + "_" + timeStamp +".png";
        File destination = new File(scrName);
        try {
            FileUtils.copyFile(scrFile,destination);
            //LOGGER.info("<br><img scr ='" + scrName + "' height ='400' width = '400'/><br>");
            LOGGER.info("Screenshot is ready " + destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scrName;
    }
}
